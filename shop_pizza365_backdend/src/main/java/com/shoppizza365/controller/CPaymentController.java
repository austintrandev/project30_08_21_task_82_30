package com.shoppizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.shoppizza365.model.*;
import com.shoppizza365.repository.*;

@RestController
public class CPaymentController {
	@Autowired
	private IPaymentRepository paymentRepository;
	
	@Autowired
	private ICustomerRepository customerRepository;
	
	@CrossOrigin
	@GetMapping("/payments")
	public ResponseEntity<List<CPayment>> getAllPayments() {
		try {
			List<CPayment> pPayments = new ArrayList<CPayment>();

			paymentRepository.findAll().forEach(pPayments::add);

			return new ResponseEntity<>(pPayments, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/payment/{id}")
	public ResponseEntity<CPayment> getPaymentById(@PathVariable("id") long id) {
		Optional<CPayment> paymentData = paymentRepository.findById(id);
		if (paymentData.isPresent()) {
			return new ResponseEntity<>(paymentData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	
	@CrossOrigin
	@GetMapping("/customer/{customer_id}/payment")
	public List<CPayment> getPaymentByCustomerId(@PathVariable Long customer_id) {
		if (customerRepository.findById(customer_id).isPresent())
			return customerRepository.findById(customer_id).get().getPayment();
		else
			return null;
	}
	@CrossOrigin
	@PostMapping("/payment/create/{id}")
	public ResponseEntity<Object> createPayment(@PathVariable("id") Long id, @RequestBody CPayment pPayment) {
		Optional<CCustomer> customerData = customerRepository.findById(id);
		if (customerData.isPresent()) {
			try {
				CPayment newPayment = new CPayment();
				newPayment.setAmmount(pPayment.getAmmount());
				newPayment.setCheckNumber(pPayment.getCheckNumber());
				newPayment.setPaymentDate(pPayment.getPaymentDate());
				newPayment.setCustomer(customerData.get());
				CPayment savedPayment = paymentRepository.save(newPayment);
				return new ResponseEntity<>(newPayment, HttpStatus.CREATED);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/payment/update/{id}")
	public ResponseEntity<Object> updatePaymentById(@PathVariable("id") long id, @RequestBody CPayment pPayment) {
		Optional<CPayment> paymentData = paymentRepository.findById(id);
		if (paymentData.isPresent()) {
			CPayment newPayment = paymentData.get();
			newPayment.setAmmount(pPayment.getAmmount());
			newPayment.setCheckNumber(pPayment.getCheckNumber());
			newPayment.setPaymentDate(pPayment.getPaymentDate());
			CPayment savedPayment = paymentRepository.save(newPayment);
			try {
				return new ResponseEntity<>(savedPayment, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Payment:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Payment: " + id + "  for update.");
		}
	}

	@CrossOrigin
	@DeleteMapping("/payment/delete/{id}")
	public ResponseEntity<CPayment> deletePaymentById(@PathVariable("id") long id) {
		try {
			paymentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/payment/checknumber/{checknumber}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumber(@PathVariable String checknumber) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumber(checknumber);
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/payment/checknumber_pageable/{checknumber}/{start}/{end}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumberPageable(@PathVariable String checknumber, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumberPageable(checknumber, PageRequest.of(start, end));
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/payment/checknumber_desc/{checknumber}/{start}/{end}")
	public ResponseEntity<List<CPayment>> findPaymentByCheckNumberDESC(@PathVariable String checknumber, @PathVariable int start,
			@PathVariable int end) {
		try {
			List<CPayment> vPayment = paymentRepository.findPaymentBycheckNumberDESC(checknumber, PageRequest.of(start, end));
			return new ResponseEntity<>(vPayment, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/payment/update_for_checknumber/{checknumber}")
	public int updateCheckNumber(@PathVariable("checknumber") String checknumber) {
		return paymentRepository.updateCheckNumber(checknumber);
	}
}
