package com.shoppizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shoppizza365.model.*;

public interface IOrderExportRepository extends JpaRepository<COrder,String> {

}
